package com.example.readfromstoragejava;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        File[] file = Environment.getRootDirectory().listFiles();

        ArrayList<File> list = new ArrayList<>();
        Collections.addAll(list, file);
        RecyclerView recyclerView;

        FileAdapter adapter = new FileAdapter(list, this);
        recyclerView = findViewById(R.id.rv);

        recyclerView.setAdapter(adapter);
    }
}
