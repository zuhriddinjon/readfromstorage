package com.example.readfromstoragejava;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.io.File;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.VH> {
    private ArrayList<File> list;
    private LayoutInflater inflater;

    public FileAdapter(ArrayList<File> list, Activity activity) {
        this.list = list;
        inflater = LayoutInflater.from(activity);
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new VH(inflater.inflate(R.layout.row_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH vh, int position) {
        vh.onBind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class VH extends RecyclerView.ViewHolder {

        private AppCompatTextView tvName, tvDate, tvSize;
        private AppCompatImageView ivAvatar;

        public VH(@NonNull View itemView) {
            super(itemView);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tvDate = itemView.findViewById(R.id.tvDate);
            this.tvSize = itemView.findViewById(R.id.tvSize);
            this.ivAvatar = itemView.findViewById(R.id.ivAvatar);
        }

        void onBind(File file) {
            tvName.setText(file.getName());
//            tvDate.setText(file.get);
//            tvSize.setText(file.getSize());
//            if (file.isDirectory()) ivAvatar.setImageResource(R.drawable.ic_folder);
//            else ivAvatar.setImageResource(R.drawable.ic_file);
            ivAvatar.setImageResource(R.drawable.ic_folder);

        }
    }
}
